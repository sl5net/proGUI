# ProGUI

ProGUI is based on BoonGUI. Main feature: allow the training of units through a dedicated panel. You decide the units you want to be produced and military buildings will training.

 ## ✨Features

-   Trainer: set the composition of units you want with mouse-wheel or toggle training of units you want to be trained whenever possible.
-   Pro Panel:
    -   Enable/Disable trainer
    -   Queue researches in forge or eco buildings
    -   Activate or not the automatic sharing of resources to allies.
    -   Ready for NextPhase: put on hold all queuing of new units and research until you have the conditions to start upgrading to next phase.
-   Boon rows:
    -   Display all idle workers
    -   Display all idle important buildings
    -   Display units in production
    -   Display all units (current army composition)
-   QuickStart:
    -   When starting with a Civic Center, units will be assigned to gather resource immediately.
    -   You can customize what units should be assigned to what resource.
-   Deeply customizable mod with settings.
  
  
## 👨‍💻 Install

System Default location for the /0ad/mods/folder

   * Linux: `~/.local/share/0ad/mods/`
   * macOS: `~/Library/Application\ Support/0ad/mods/`
   * Windows: `~\Documents\My Games\0ad\mods\`

Unpack root zip in your /0ad/mods/ folder.
Start 0 A.D., click Settings and Mod Selection.
Double-click boonGUI/proGUI, click Save Configuration and Start Mods.

Based on BoonGUI: https://github.com/LangLangBart/boonGUI

## Basics

### Overlay

The right panel, display the following lists:

- **Idle units**
- **Idle production buildings**
- **Current productions**
- **Army composition**

![IMAGE_DESCRIPTION](https://i.ibb.co/R4WQbt1/example.png)

Helpfull to select "all idle baracks" or "idle forges" in a click.

### Trainer

Activate trainer by clicking on this icon on the right panel

![IMAGE_DESCRIPTION](https://i.ibb.co/qJ93ptR/Screenshot-from-2023-05-04-16-19-29.png)

Select the units you want your buildings to produce:

![IMAGE_DESCRIPTION](https://i.ibb.co/W3kMcNk/Screenshot-from-2023-05-04-16-33-01.png)

With defaults settings and this configuration, Civic Center will train females and Baracks will train archers.

Other exemple with females and spears:

![IMAGE_DESCRIPTION](https://i.ibb.co/gTycpg5/example2.png)

### Force a building to produce a unit type

Your Civic Center is not training the unit you want it to?

When trainer is on, you can force a building to produce a type of unit, regardless of trainer settings, by activating vanilla native game feature "auto-queue".

The auto-queue is slighty modified: the trainer will resize the batch you've set in function of resources, for the queue not to break.

I can play without ever having to use this feature, **see section below for better levreaging of the composition feature**.

### Configuration for trainer 

For trainer, you have a bunch of options to customize how you will define units to train in game. The goal is for you to be able to change it, to whatever you think is the most intuitive.

Default settings are more intuitive (for most) and faster to manage in game.

_If you have a MouseWheel_ try the settings below instead:

![IMAGE_DESCRIPTION](https://wildfiregames.com/forum/uploads/monthly_2023_05/2023_05_03_07h_Kleki.thumb.png.20e15183be2025c350fb8fbf7228cbd8.png)

Then, use mousewheel on the icon units tab to set desiered composition:

![IMAGE_DESCRIPTION](https://i.ibb.co/F5LJRsK/Screenshot-from-2023-05-04-16-55-26.png)


