class BoonGUITechPanelTab {
	constructor(tab, index, parent) {
		const mode = BoonGUITechPanel.Modes[index] ?? null;
		this.tab = tab;
		this.index = index;

		this.sellPrice = Engine.GetGUIObjectByName(`${tab.name}_Count`);
		this.orderCount = Engine.GetGUIObjectByName(`${tab.name}_Order`);
		const name = Engine.GetGUIObjectByName(`${tab.name}_Name`);
		this.bg = Engine.GetGUIObjectByName(`${tab.name}_Background`);
		this.bgState = Engine.GetGUIObjectByName(`${tab.name}_BackgroundState`);
		const toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "boongui.rightPanel.color") + ".dds";
		this.bg.sprite = toggledIcon;
		this.orderCount.caption = 0;
		this.orderCount.hidden = true;

		this.isChecked = Engine.GetGUIObjectByName(`${tab.name}`).checked;
		if (mode) {
			name.caption = mode.type;
			tab.hidden = false;
			tab.size = BoonGUIGetColSize(index, 33);
			const icon = Engine.GetGUIObjectByName(`${tab.name}_Icon`);

			icon.sprite = `stretched:session/portraits/technologies/${mode.icon}.png`;
			icon.size = "2 3 100%-2 100%-4"

			tab.tooltip = colorizeHotkey(`${mode.title} %(hotkey)s`);
			tab.onPress = () => {
				parent.toggleMode(index);
			};
			this.isChecked = true;
			this.shouldBeHidden = false;
		}
		else {
			this.shouldBeHidden = true;
		}
		tab.hidden = true;

	}

	update(toToggleIndex) {

		if (toToggleIndex === this.index) {

			this.isChecked = !this.isChecked;
		}
		if (this.isChecked === true) {
			const toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "boongui.rightPanel.color") + ".dds";
			this.bg.sprite = toggledIcon;
		}
		else if (this.isChecked === false) {
			this.bg.sprite = `stretched:session/icons/bkg/portrait_black.dds`;
		}
	}
	hide(boolean) {
		if (!this.shouldBeHidden)
			this.tab.hidden = boolean;
	}
}
