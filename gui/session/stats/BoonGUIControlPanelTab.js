class BoonGUIControlPanelTab {
	constructor(tab, index, parent) {
		const mode = BoonGUIControlPanel.Modes[index] ?? null;
		this.tab = tab;
		this.index = index;

		this.sellPrice = Engine.GetGUIObjectByName(`${tab.name}_Count`);
		this.orderCount = Engine.GetGUIObjectByName(`${tab.name}_Order`);
		const name = Engine.GetGUIObjectByName(`${tab.name}_Name`);
		this.bg = Engine.GetGUIObjectByName(`${tab.name}_Background`);
		this.bgState = Engine.GetGUIObjectByName(`${tab.name}_BackgroundState`);
		const toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "boongui.rightPanel.color") + ".dds";
		this.bg.sprite = toggledIcon;
		this.orderCount.caption = 0;
		this.orderCount.hidden = true;

		this.isChecked = Engine.GetGUIObjectByName(`${tab.name}`).checked;
		if (mode) {
			name.caption = mode.type;
			tab.hidden = false;
			tab.size = BoonGUIGetColSize(index, 33);
			const icon = Engine.GetGUIObjectByName(`${tab.name}_Icon`);

			if (this.index > 3 && this.index != 8) {
				if (Engine.ConfigDB_GetValue("user", "boongui.barter.isEnabled") == "true")
					icon.sprite = `stretched:session/icons/resources/${mode.icon}.png`;
				else
					this.tab.hidden = true;
			}

			else
				icon.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/${mode.icon}.png`;

			tab.tooltip = colorizeHotkey(`${mode.title} %(hotkey)s`, `boongui.session.controlPanel.${index + 1}`);
			tab.onPress = () => {
				parent.toggleMode(index);
			};
			if (this.index > 3)
				this.isChecked = false;
			else if (Engine.ConfigDB_GetValue("user", `boongui.controlPanel.${index}`) === "false")
				this.isChecked = false;
		}

		//Define settings options for certain tabs
		switch (this.index * 1) {

			case 0: //Trainer Toggle TODO function
				tab.onMouseRightPress = () => {
					g_stats.reservePanel.hideShowTabs();
					g_stats.techPanel.hideShowTabs(true); //Hide other tabs
					g_stats.ecoTechPanel.hideShowTabs(true);
					g_stats.tributePanel.hideShowTabs(true);
				};
				break;
			case 1: //Tech Toggle

				tab.onMouseRightPress = () => {
					g_stats.techPanel.hideShowTabs();
					g_stats.reservePanel.hideShowTabs(true);
					g_stats.ecoTechPanel.hideShowTabs(true);
					g_stats.tributePanel.hideShowTabs(true);
				};
				break;
			case 2: //Eco Tech Toggle

				tab.onMouseRightPress = () => {
					g_stats.ecoTechPanel.hideShowTabs();
					g_stats.reservePanel.hideShowTabs(true);
					g_stats.techPanel.hideShowTabs(true);
					g_stats.tributePanel.hideShowTabs(true);
				};
				break;
			case 3: //Tribute Toggle

				tab.onMouseRightPress = () => {
					g_stats.tributePanel.hideShowTabs();
					g_stats.ecoTechPanel.hideShowTabs(true);
					g_stats.reservePanel.hideShowTabs(true);
					g_stats.techPanel.hideShowTabs(true);
				};
				break;
			default:
				tab.onMouseRightPress = () => {
					g_stats.tributePanel.hideShowTabs(true);
					g_stats.reservePanel.hideShowTabs(true);
					g_stats.techPanel.hideShowTabs(true);
					g_stats.ecoTechPanel.hideShowTabs(true);
				};
		}
	}

	update(toToggleIndex, isPaused) {

		if (toToggleIndex === this.index) {

			this.isChecked = !this.isChecked;
			if (this.index <= 3)
				toggleConfigBool(`boongui.controlPanel.${this.index}`);
		}
		if (this.isChecked === true) {
			const toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "boongui.rightPanel.color") + ".dds";
			this.bg.sprite = toggledIcon;
		}
		else if (this.isChecked === false) {
			this.bg.sprite = `stretched:session/icons/bkg/portrait_black.dds`;
		}


		if ((isPaused && this.index != 8) || this.isChecked === false)
			this.bgState.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/pause.png`;
		else if ((this.isChecked === true && !isPaused))
			this.bgState.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/play.png`;
		if (this.index == 8 && this.isChecked)
			this.bgState.sprite = `stretched:color:dimmedWhite:textureAsMask:session/phosphor/play.png`;
		Engine.GetGUIObjectByName(`${this.tab.name}`).checked = this.isChecked;

	}
}
