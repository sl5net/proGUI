class EcoHelp {

    constructor() {
        this.modes = new Array;
        this.allyToTribute = new Array;
        this.playerEntities = this.getPlayerEntities();
        this.playersStates = this.getPlayersStates();
        this.hasBeenUpdated = false;
        this.timeOfLastOrder = 3000;
        this.timeOfLastBarter = 3000;
        this.lastTrainerOrderedID = 0;
        this.hasMarket = false;
        this.setAutoQueue = new Array;
        this.lastPriceUpdate = {
            "food": 0,
            "wood": 0,
            "stone": 0,
            "metal": 0
        };
        this.resource = {
            "counts": {},
            "gatherers": {},
            "rates": {}
        };
        this.userRefRes = {
            "food": 0,
            "wood": 0,
            "stone": 0,
            "metal": 0
        };
        this.modes = this.getModes();
        this.unitsToIgnore = 0;
    }
    getModes() {
        const modes = [];
        const numModes = Engine.GetGUIObjectByName("StatsModesTabButtons").children.length;
        const prefix = "stretched:session/portraits/";
        for (let i = 0; i < numModes; i++) {
            const unitFromTab = Engine.GetGUIObjectByName(`StatsModesTabButton[${i}]_Name`);
            let name = unitFromTab.caption;

            const value = Number.parseFloat(Engine.GetGUIObjectByName(`StatsModesTabButton[${i}]_Count`).caption);
            const toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "boongui.rightPanel.color") + ".dds";
            const checked = (Engine.GetGUIObjectByName(`StatsModesTabButton[${i}]_Background`).sprite == toggledIcon) ? true : false;
            modes.push({
                name,
                value,
                checked,
                count: 0
            });
        }
        return modes;
    }

    getUnspendableResources() {
        return {

            "food": Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.foodReserve")),
            "wood": Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.woodReserve")),
            "stone": Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.stoneReserve")),
            "metal": Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.metalReserve"))
        }
    }
    getPlayersStates() {
        const playersStates = Engine.GuiInterfaceCall("boongui_GetOverlay", {
            g_IsObserver, g_ViewedPlayer, g_LastTickTime
        }).players ?? [];
        return playersStates;
    }
    updateMarketPrices() {
        let i = 0;
        let prices = GetSimState().players[g_ViewedPlayer].barterPrices;
        for (const resType of g_BoonGUIResTypes) {
            this.sellPrice = Engine.GetGUIObjectByName(`ControlPanelTabButton[0][${4 + i}]_Count`);
            i++;
            if (Number.parseFloat(this.sellPrice.caption) == Math.round(prices.sell[resType]) && g_SimState.timeElapsed > this.lastPriceUpdate[resType] + 600)
                this.sellPrice.textcolor = "255 255 255";
            else if (Number.parseFloat(this.sellPrice.caption) != Math.round(prices.sell[resType])) {
                this.sellPrice.textcolor = (this.sellPrice.caption < prices.sell[resType]) ? "55 255 55" : "255 55 55";
                this.lastPriceUpdate[resType] = g_SimState.timeElapsed;
            }
            this.sellPrice.caption = Math.round(prices.sell[resType]);
            this.sellPrice.hidden = false;

        }
    }
    // This function handles the bartering of resources between players.
    barterResources() {
        // Only execute the function if it has been updated and enough time has passed since the last barter.
        if (this.hasBeenUpdated && this.timeOfLastBarter + Math.round(Engine.ConfigDB_GetValue("user", "boongui.barter.barterDealy")) < g_SimState.timeElapsed) {
            let i = 0;

            // Retrieve the current player's barter prices.
            let prices = GetSimState().players[g_ViewedPlayer].barterPrices;

            // Iterate through each resource type that can be sold.
            for (const resTypeSell of g_BoonGUIResTypes) {
                // Retrieve the sell order count for the current resource type.
                this.orderSellCount = Engine.GetGUIObjectByName(`ControlPanelTabButton[0][${4 + i}]_Order`);
                const toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "boongui.rightPanel.color") + ".dds";
                const canBeSold = true;

                if (canBeSold) {
                    let y = 0;

                    // Iterate through each resource type that can be bought.
                    for (const resTypeBuy of g_BoonGUIResTypes) {
                        // Determine whether the current resource type can be bought.
                        const canBeBought = Engine.GetGUIObjectByName(`ControlPanelTabButton[0][${4 + y}]_Background`).sprite == toggledIcon;
                        this.orderBuyCount = Engine.GetGUIObjectByName(`ControlPanelTabButton[0][${4 + y}]_Order`);

                        // Calculate the minimum difference between the sell and buy prices required to initiate a trade.
                        const minimumDelta = (Engine.ConfigDB_GetValue("user", "boongui.barter.lowRes") > this.userStates.resourceCounts[resTypeBuy]) ? 30 + Math.round(Engine.ConfigDB_GetValue("user", "boongui.barter.maxDelta")) : 30 + Math.round(Engine.ConfigDB_GetValue("user", "boongui.barter.minDelta"));

                        // Initiate a trade if the conditions are met.
                        if (canBeBought && prices.sell[resTypeSell] - prices.sell[resTypeBuy] > minimumDelta
                            && this.userStates.resourceCounts[resTypeSell] > Engine.ConfigDB_GetValue("user", "boongui.barter.reserve")) {
                            Engine.PostNetworkCommand({
                                "type": "barter",
                                "sell": resTypeSell,
                                "buy": resTypeBuy,
                                "amount": 100
                            });

                            // Update the sell and buy order counts.
                            let countBuyOrder = Number.parseFloat(this.orderBuyCount.caption);
                            let countSellOrder = Number.parseFloat(this.orderSellCount.caption);
                            this.hasBeenUpdated = false;
                            this.orderBuyCount.hidden = false;
                            this.orderSellCount.hidden = false;
                            this.orderBuyCount.caption = countBuyOrder * 1 + 100;
                            this.orderBuyCount.textcolor = "55 255 55"
                            this.orderSellCount.caption = countSellOrder * 1 - 100;
                            this.orderSellCount.textcolor = "255 55 55"

                            // Set the time of the last barter and order to the current simulation time.
                            this.timeOfLastBarter = g_SimState.timeElapsed;
                            this.timeOfLastOrder = g_SimState.timeElapsed;
                        }

                        // Hide the buy order count if too much time has passed since the last barter.
                        if (this.timeOfLastBarter + Math.round(Engine.ConfigDB_GetValue("user", "boongui.barter.barterLookUpTime")) < g_SimState.timeElapsed) {
                            this.orderBuyCount.caption = 0;
                            this.orderBuyCount.hidden = true;
                        }
                        y++;
                    }

                }
                i++;
            }
        }
    }

    // This function is responsible for executing a series of tasks during gameplay.
    tickTasks() {
        // Execute these tasks only if the player is not an observer.
        if (!g_IsObserver) {
            // Update the toggledIcon whenever the color changes.
            const toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "boongui.rightPanel.color") + ".dds";
            // Check if preparing for next phase.
            this.preparingNextPhase = Engine.GetGUIObjectByName(`ControlPanelTabButton[0][8]_Background`).sprite == toggledIcon;

            // Get the states of all players and filter by the currently viewed player.
            this.playersStates = this.getPlayersStates();
            this.userStates = this.playersStates.find(x => x.index === g_ViewedPlayer);

            // If not preparing for next phase, perform these tasks.
            if (this.preparingNextPhase == false) {
                // Update market prices, get modes, and get player entities.
                this.updateMarketPrices();
                this.modes = this.getModes();
                this.playerEntities = this.getPlayerEntities();

                // If there is a market, barter resources and check for updates.
                if (this.hasMarket == true) {
                    this.barterResources();
                    this.checkIfUpdated();
                }

                // If the control panel tab button icon matches the toggledIcon, train citizens and check for updates.
                if (Engine.GetGUIObjectByName(`ControlPanelTabButton[0][0]_Background`).sprite == toggledIcon) {
                    this.trainCitizens();
                    this.checkIfUpdated();
                }

                // Research technologies and check for updates.
                this.researchTechnologies();
                this.checkIfUpdated();

                // If the control panel tab button icon matches the toggledIcon, help allies tribute and check for updates.
                if (Engine.GetGUIObjectByName(`ControlPanelTabButton[0][3]_Background`).sprite == toggledIcon) {
                    this.helpAlliesTribute();
                    this.checkIfUpdated();
                }
            }
            // If preparing for next phase, perform these tasks.
            else if (this.preparingNextPhase == true) {
                // Get player entities and researcher technologies.
                this.playerEntities = this.getPlayerEntities();
                if (this.playerEntities.CivilCentre[0]) {
                    let playerEntity = this.playerEntities.CivilCentre[0];
                    let state = GetEntityState(playerEntity);
                    let reseachableTechs = state.researcher.technologies;

                    // If a researchable technology is available, make the research and toggle the control panel mode.
                    if (reseachableTechs[0]) {
                        let tech = reseachableTechs[0];
                        if (this.makeResearch(playerEntity, tech, true)) {
                            g_stats.controlPanel.toggleMode(8, true);
                        }
                    }
                }
            }
        }
    }

    // This function checks if an entity has been updated recently by comparing its state to the previous time it was ordered.
    checkIfUpdated() {
        // Get the state of the last trainer that was ordered.
        let state = GetEntityState(this.lastTrainerOrdered);

        // Check if the elapsed game time is greater than the time of the last order.
        if (g_SimState.timeElapsed > this.timeOfLastOrder) {
            // If the last trainer ordered no longer exists, set hasBeenUpdated to true.
            if (!GetEntityState(this.lastTrainerOrdered))
                this.hasBeenUpdated = true;
            // Otherwise, check if there are items in the production queue or if enough time has passed since the last order to set hasBeenUpdated to true.
            else if ((state.production.queue.length != 0 && state.production.queue[0].timeRemaining > 1000) || g_SimState.timeElapsed - this.timeOfLastOrder > 1200)
                this.hasBeenUpdated = true;
        }
    }

    // This function returns the number of idle trainers by checking their production queue status
    getNumberOfIdleTrainers() {
        let num = 0;
        try {
            // Loop through all the Trainer entities owned by the player
            for (let trainerID in this.playerEntities.Trainer) {
                // Get the state of each Trainer entity
                let state = GetEntityState(this.playerEntities.Trainer[trainerID]);

                // Check if the production queue of the Trainer entity is empty or if there is only one item in the queue and its progress is greater than 0.9
                if (state.production.queue.length === 0 || (state.production.queue.length === 1 && state.production.queue[0].progress > 0.9)) {
                    // If the Trainer is idle, increment the count
                    num++;
                }
            }
        } catch (error) {
            // Catch any errors that may occur during execution
            warn('An error occurred while retrieving the number of idle Trainers:', error);
            return -1; // Return -1 to indicate an error occurred
        }
        // Return the number of idle Trainers
        return num;
    }

    setNotToTribute(playerNick) {
        this.allyToTribute = this.allyToTribute.filter(x => x != playerNick);
    }
    setToTribute(playerNick) {
        this.allyToTribute.push(playerNick);
    }
    // This function is for tributing resources to allied players.
    helpAlliesTribute() {
        // Define the image path for the toggled icon based on user config
        const toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "boongui.rightPanel.color") + ".dds";

        // Check if the user states have been updated and if player states exist
        if (this.userStates && this.hasBeenUpdated == true) {
            // Loop through each player state
            for (let playerStates of this.playersStates) {
                let state = playerStates;
                // Check if the player is in the list of allies to tribute
                if (this.allyToTribute.indexOf(state.name.split(' ')[0]) != -1) {
                    // Loop through each resource type to be shared
                    for (const resType of g_BoonGUIResTypes) {
                        // Get the current amount of the resource for the player being checked and the user
                        let value = state.resourceCounts[resType];
                        let userRes = this.userStates.resourceCounts[resType];
                        let userGatherers = this.userStates.resourceGatherers[resType];
                        let shareratio = 0;

                        // Get the share ratio for the resource based on the toggled icon and resource type
                        if (resType == "food")
                            shareratio = (Engine.GetGUIObjectByName(`ControlPanelTabButton[4][0]_Background`).sprite == toggledIcon) ? Engine.GetGUIObjectByName(`ControlPanelTabButton[4][0]_Count`).caption : 0;
                        else if (resType == "wood")
                            shareratio = (Engine.GetGUIObjectByName(`ControlPanelTabButton[4][1]_Background`).sprite == toggledIcon) ? Engine.GetGUIObjectByName(`ControlPanelTabButton[4][1]_Count`).caption : 0;
                        else if (resType == "stone")
                            shareratio = (Engine.GetGUIObjectByName(`ControlPanelTabButton[4][2]_Background`).sprite == toggledIcon) ? Engine.GetGUIObjectByName(`ControlPanelTabButton[4][2]_Count`).caption : 0;
                        else if (resType == "metal")
                            shareratio = (Engine.GetGUIObjectByName(`ControlPanelTabButton[4][3]_Background`).sprite == toggledIcon) ? Engine.GetGUIObjectByName(`ControlPanelTabButton[4][3]_Count`).caption : 0;

                        // Check if the user resource count has changed
                        if (this.userRefRes[resType] != userRes) {
                            // Check if both the player being checked and the user have resources and gatherers for the resource type
                            if (userRes && userGatherers) {
                                // Calculate the gatherer ratio of the player being checked to the user
                                let gatherratio = (state.resourceGatherers[resType] + 1) / (userGatherers + 1);
                                // Get the tribute reserve specified in the config
                                const resReserve = Math.floor(Engine.ConfigDB_GetValue("user", "boongui.autoTribute.tributeReserve"));

                                // Calculate the optimal amount to tribute based on share ratio, player resources and gatherers, and user resources
                                if (value * gatherratio < userRes && userRes > resReserve && Math.round(((userRes - value - resReserve) * shareratio) / 3) > 100 + state.resourceGatherers[resType]) {
                                    // If the optimal amount to tribute is greater than 100, send a tribute command
                                    let playerID = state.index;
                                    this.userRefRes[resType] = userRes;
                                    Engine.PostNetworkCommand({
                                        "type": "tribute",
                                        "player": playerID,
                                        "amounts": {
                                            [resType]: Math.round(((userRes - value - resReserve) * shareratio) / 3 + userGatherers)
                                        }
                                    });
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    getRemainingUnitsToTrain(template) {
        const matchingMode = this.modes.find(x => x.name === template);
        if (matchingMode) {
            if (matchingMode.checked)
                return this.userStates.popLimit * 1;
            let currentRatio = matchingMode.count / (this.userStates.popCount - this.unitsToIgnore);
            if (currentRatio > matchingMode.value / 100) {
                if (matchingMode.value >= 1) {
                    let shouldOverTrain = (Engine.ConfigDB_GetValue("user", "boongui.trainer.strictCompo") == "true") ? 0 : 0.5;
                    return shouldOverTrain;
                }

                else
                    return 0;
            }
            return this.userStates.popLimit * (matchingMode.value / 100) - matchingMode.count;
        } else {
            return 0;
        }
    }

    getTrainableUnits(template) {
        let unitCost = GetTemplateData(template).cost;
        let trainableUnits = Math.floor(this.userStates.popLimit - this.userStates.popCount / unitCost.population);
        let idleTrainers = this.getNumberOfIdleTrainers();
        const maxTrainingBatchSize = Math.floor(Engine.ConfigDB_GetValue("user", "boongui.trainer.MaxBatchSize"));
        let i = 0;
        for (const resType of g_BoonGUIResTypes) {
            const toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "boongui.rightPanel.color") + ".dds";
            let unspendatbleResources = (Engine.GetGUIObjectByName(`ControlPanelTabButton[1][${i}]_Background`).sprite == toggledIcon) ? Engine.GetGUIObjectByName(`ControlPanelTabButton[1][${i}]_Count`).caption : 0;
            let spendableRes = this.userStates.resourceCounts[resType] - unspendatbleResources;
            if (spendableRes < 0)
                spendableRes = 0;
            if (trainableUnits > spendableRes / unitCost[resType]) {
                trainableUnits = Math.floor(spendableRes / unitCost[resType]);
            }
            i++;
        }
        if (trainableUnits >= idleTrainers)
            trainableUnits = Math.ceil(trainableUnits / idleTrainers);
        if (trainableUnits > Math.ceil(maxTrainingBatchSize / (unitCost.population)))
            trainableUnits = Math.ceil(maxTrainingBatchSize / (unitCost.population));

        return trainableUnits;
    }
    canResearch(unitCost) {

        for (const resType of g_BoonGUIResTypes) {
            let spendableRes = this.userStates.resourceCounts[resType];
            if (spendableRes < 0)
                spendableRes = 0;
            if (unitCost[resType] > spendableRes) {
                return false;
            }
        }
        return true;
    }
    makeResearch(entity, tech, priority) {
        let isTechnologyResearchable = Engine.GuiInterfaceCall("CheckTechnologyRequirements", {
            "tech": tech,
            "player": this.userStates.index
        });
        let state = GetEntityState(entity);
        if (priority == true && state?.production?.queue[0]?.timeRemaining != undefined)
            if (state.production.queue[0].timeRemaining <= Math.round(Engine.ConfigDB_GetValue("user", "boongui.barter.pushOrderIfFrontTimeRemaining")))
                priority = false;
        let hasResForResearch = this.canResearch(TechnologyTemplates.Get(tech).cost);
        if (hasResForResearch && isTechnologyResearchable) {
            Engine.PostNetworkCommand({
                "type": "research",
                "entity": entity,
                "template": tech,
                "pushFront": priority
            });
            this.hasBeenUpdated = false;
            this.lastTrainerOrdered = entity;
            this.timeOfLastOrder = g_SimState.timeElapsed;
            return true;

        }
        return false;
    }
    researchTechnologies() {

        research:
        if (this.hasBeenUpdated == true) {
            const toggledIcon = "stretched:session/icons/bkg/portrait_" + Engine.ConfigDB_GetValue("user", "boongui.rightPanel.color") + ".dds";
            for (let researcherID in this.playerEntities.Researcher) {
                let playerEntity = this.playerEntities.Researcher[researcherID]
                let state = GetEntityState(playerEntity);
                if (state.researcher
                    && this.userStates && (state.production.queue.length === 0 || (state.production.queue.length === 1 && state.production.queue[0].timeRemaining <= 1000))) {
                    let reseachableTechs = JSON.parse(JSON.stringify(state.researcher.technologies));
                    //forge:
                    if (Engine.GetGUIObjectByName(`ControlPanelTabButton[0][1]_Background`).sprite == toggledIcon) {
                        if (state.identity.classes.indexOf("Forge") !== -1) {

                            for (let tech of reseachableTechs.reverse()) {
                                if (state.researcher.technologies.indexOf(tech) != -1)
                                    if (Engine.GetGUIObjectByName(`ControlPanelTabButton[2][${state.researcher.technologies.indexOf(tech)}]_Background`).sprite == toggledIcon)
                                        if (tech !== null && tech != "archer_attack_spread") {
                                            if (this.makeResearch(playerEntity, tech, false))
                                                break research;
                                        }
                            }
                        }
                    }

                    if (Engine.GetGUIObjectByName(`ControlPanelTabButton[0][2]_Background`).sprite == toggledIcon) {
                        //Storehouse:
                        let triggerWoodAmount = Math.round(parseFloat(Engine.ConfigDB_GetValue("user", "boongui.ecohelp.woodStorehouseP1")));
                        if (Engine.GetGUIObjectByName(`ControlPanelTabButton[3][0]_Background`).sprite == toggledIcon) {
                            if ((hasClass(state, "Storehouse") && (this.userStates.phase != "village" || this.userStates.resourceCounts.wood > triggerWoodAmount) && reseachableTechs[0])) {
                                let tech = reseachableTechs[0];
                                if (this.makeResearch(playerEntity, tech, false))
                                    break research;
                            }
                        }
                        if (Engine.GetGUIObjectByName(`ControlPanelTabButton[3][2]_Background`).sprite == toggledIcon) {
                            if ((hasClass(state, "Storehouse") && reseachableTechs[1])) {
                                let tech = reseachableTechs[1];
                                if (this.makeResearch(playerEntity, tech, false))
                                    break research;
                            }
                        }
                        if (Engine.GetGUIObjectByName(`ControlPanelTabButton[3][3]_Background`).sprite == toggledIcon)
                            if ((hasClass(state, "Storehouse") && reseachableTechs[2])) {
                                let tech = reseachableTechs[2];
                                if (this.makeResearch(playerEntity, tech, false))
                                    break research;
                            }
                        triggerWoodAmount = Math.round(parseFloat(Engine.ConfigDB_GetValue("user", "boongui.ecohelp.woodFarmP1")));
                        //farmstead: 
                        if (Engine.GetGUIObjectByName(`ControlPanelTabButton[3][1]_Background`).sprite == toggledIcon) {
                            if ((hasClass(state, "Farmstead") && (this.userStates.phase != "village" || this.userStates.resourceCounts.wood > triggerWoodAmount) && reseachableTechs[1])) {
                                let tech = reseachableTechs[1];
                                if (this.makeResearch(playerEntity, tech, false))
                                    break research;
                            }
                        }
                        if (Engine.GetGUIObjectByName(`ControlPanelTabButton[3][4]_Background`).sprite == toggledIcon) {

                            if ((hasClass(state, "Farmstead") && reseachableTechs[0])) {
                                let tech = reseachableTechs[0];
                                if (this.makeResearch(playerEntity, tech, false))
                                    break research;
                            }
                        }
                    }
                }
            }
        }
    }
    trainCitizens() {
        if (this.hasBeenUpdated == true) {

            for (let trainerID in this.playerEntities.Trainer) {

                let state = GetEntityState(this.playerEntities.Trainer[trainerID]);
                let unitToTrain = false;
                let unitToTrainQty = 0;
                let unitToTrainQtyMax = false;
                if (state?.production?.queue[0]?.unitTemplate != undefined || state?.production?.queue.length === 0) {
                    if (state.trainer && this.userStates) {
                        let trainableUnitsList = (Engine.ConfigDB_GetValue("user", "boongui.trainer.reversePriority") == "true") ? JSON.parse(JSON.stringify(state.trainer.entities)) : JSON.parse(JSON.stringify(state.trainer.entities)).reverse();

                        for (let trainableUnit of trainableUnitsList) {
                            let matchingMode = this.modes.find(x => x.name == trainableUnit);
                            if (matchingMode != undefined) {

                                if (GetTemplateData(trainableUnit).requiredTechnology) {

                                    let technologyEnabled = Engine.GuiInterfaceCall("IsTechnologyResearched", {
                                        "tech": GetTemplateData(trainableUnit).requiredTechnology,
                                        "player": this.userStates.index
                                    });
                                    if (!technologyEnabled)
                                        continue;
                                }
                                let thisToTrainQty = this.getTrainableUnits(trainableUnit);
                                let thisToTrainQtyMax = this.getRemainingUnitsToTrain(trainableUnit);
                                if (thisToTrainQtyMax != false) {
                                    if (thisToTrainQtyMax < thisToTrainQty) {
                                        thisToTrainQty = thisToTrainQtyMax;

                                    }
                                }

                                if (((thisToTrainQty >= unitToTrainQty || (matchingMode.checked && unitToTrainQty >= 1)) && thisToTrainQtyMax > 0)) {

                                    unitToTrain = trainableUnit;
                                    unitToTrainQty = this.getTrainableUnits(trainableUnit); //Do not limite bach for compo
                                    unitToTrainQtyMax = this.getRemainingUnitsToTrain(trainableUnit);
                                    if (matchingMode.checked && unitToTrainQty >= 1)
                                        break;

                                }

                            }

                        }

                        // Auto-Queue modifier
                        if (this.setAutoQueue.findIndex(x => x.id == trainerID) != -1) {

                            unitToTrain = this.setAutoQueue.find(x => x.id == trainerID).unit;

                            unitToTrainQty = this.getTrainableUnits(unitToTrain);
                            if (!state.production.autoqueue && this.setAutoQueue.find(x => x.id == trainerID)?.queue == true) {
                                Engine.PostNetworkCommand({
                                    "type": "autoqueue-on",
                                    "entities": JSON.parse("[" + JSON.stringify(this.playerEntities.Trainer[trainerID]) + "]")
                                });
                                this.setAutoQueue.find(x => x.id == trainerID).queue = false;
                            }
                            if (unitToTrainQty >= 1)
                                this.setAutoQueue = this.setAutoQueue.filter(x => x.id != trainerID);

                        }
                        else if (state.production?.autoqueue && state.production?.queue[0]) {
                            unitToTrain = state.production.queue[0].unitTemplate;
                            unitToTrainQty = this.getTrainableUnits(unitToTrain);
                        }
                        // End of Auto-Queue modifier

                    }
                    if (unitToTrain != false) {



                        if ((state.production.queue.length === 0 || ((state.production.queue.length === 1 || state.production.autoqueue) && state.production.queue[0].timeRemaining <= 1000))) {

                            // Auto-Queue modifier
                            if (state.production.autoqueue && state.production.queue.length != 0) {
                                Engine.PostNetworkCommand({
                                    "type": "autoqueue-off",
                                    "entities": JSON.parse("[" + JSON.stringify(this.playerEntities.Trainer[trainerID]) + "]")
                                });
                                if (this.setAutoQueue.findIndex(x => x.id == trainerID && x.unit == unitToTrain) === -1) {
                                    this.setAutoQueue = this.setAutoQueue.filter(x => x.id != trainerID);
                                    this.setAutoQueue.push({ "id": trainerID, "unit": state.production.queue[0].unitTemplate, "queue": true });
                                }
                                else if (this.setAutoQueue.findIndex(x => x.id == trainerID && x.unit == unitToTrain) != -1 && this.setAutoQueue.find(x => x.id == trainerID && x.unit == unitToTrain).queue == false) {
                                    this.setAutoQueue.find(x => x.id == trainerID && x.unit == unitToTrain).queue = true;
                                }
                                unitToTrain = state.production.queue[0].unitTemplate;
                                unitToTrainQty = this.getTrainableUnits(unitToTrain);
                            }
                            // End of Auto-Queue modifier

                            const template = unitToTrain;
                            let trainableUnits = unitToTrainQty;
                            if (GetTemplateData(template).visibleIdentityClasses.indexOf("Hero") != -1)
                                trainableUnits = 1;
                            if (trainableUnits >= 1) {
                                const batchMultiple = Math.round(Engine.ConfigDB_GetValue("user", "boongui.trainer.batchMultiple"));
                                trainableUnits = (trainableUnits < batchMultiple) ? trainableUnits : trainableUnits - (Math.floor(trainableUnits % batchMultiple));
                                this.hasBeenUpdated = false;
                                this.lastTrainerOrdered = this.playerEntities.Trainer[trainerID];
                                this.timeOfLastOrder = g_SimState.timeElapsed;
                                Engine.PostNetworkCommand({
                                    "type": "train",
                                    "entities": JSON.parse("[" + JSON.stringify(this.playerEntities.Trainer[trainerID]) + "]"),
                                    "template": template,
                                    "count": trainableUnits,
                                    "pushFront": false
                                });
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    // Return an object of player entities
    getPlayerEntities() {
        this.unitsToIgnore = 0;
        let playerEntities = {
            CivilCentre: [],
            Trainer: [],
            Researcher: []
        };
        for (let mode in this.modes) {
            this.modes[mode].count = 0;
        }
        const interfacePlayerEntities = Engine.GuiInterfaceCall("GetPlayerEntities");
        this.hasMarket = false;
        for (let entityId of interfacePlayerEntities) {
            let state = GetEntityState(entityId);
            let templatedata = GetTemplateData(state.template);
            const matchingMode = this.modes.find(x => x.name == state.template);

            if (matchingMode != undefined) {
                matchingMode.count++;
                if (matchingMode.checked == true || matchingMode.value == 0) {
                    if (templatedata.cost != undefined)
                        if (templatedata.cost.population > 0)
                            this.unitsToIgnore += templatedata.cost.population * 1;
                }
            }
            else if (templatedata.cost != undefined)
                if (templatedata.cost.population > 0)
                    this.unitsToIgnore += templatedata.cost.population * 1;


            if (state?.production?.queue[0] != undefined) {
                if (state.production.queue[0].unitTemplate != undefined) {
                    let unitTemplateInProduction = state.production.queue[0].unitTemplate;
                    if (GetTemplateData(unitTemplateInProduction)?.cost?.population != undefined) {

                        let popCostUnitInProduction = GetTemplateData(unitTemplateInProduction).cost.population;
                        let trainerMatchingMode = this.modes.find(x => x.name == unitTemplateInProduction);

                        if ((trainerMatchingMode != undefined) && popCostUnitInProduction > 0) {
                            if (trainerMatchingMode.value > 0)
                                trainerMatchingMode.count += state.production.queue[0].count * popCostUnitInProduction;
                            else
                                this.unitsToIgnore += state.production.queue[0].count * popCostUnitInProduction;
                        }
                        else if (popCostUnitInProduction > 0)
                            this.unitsToIgnore += state.production.queue[0].count * popCostUnitInProduction;
                    }
                }
            }

            for (let key in playerEntities) {

                if (hasClass(state, "Foundation"))
                    break;
                if (hasClass(state, "Market"))
                    this.hasMarket = true;
                if (hasClass(state, "Economic") || hasClass(state, "Forge")) {
                    playerEntities["Researcher"].push(entityId);
                    if (!hasClass(state, "Dock"))
                        break;
                }
                if (hasClass(state, "CivilCentre") || hasClass(state, "Military") || hasClass(state, "Dock") || hasClass(state, "Temple")) {
                    playerEntities["Trainer"].push(entityId);
                    if (hasClass(state, "CivilCentre"))
                        playerEntities["CivilCentre"].push(entityId);
                    break;
                }


            }
        }

        return playerEntities;
    }

}

