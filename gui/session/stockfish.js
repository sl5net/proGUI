class stockfish {
    constructor() {
        this.garrisonEnt = new Array();
        this.turretEnt = new Array();
        this.turbo = false;
    }
    toggleTurbo() {
        this.turbo = !this.turbo;
        let onOrOff = (this.turbo) ? "ON" : "OFF";
        warn("Seeh chains turbo " + onOrOff + "!");
    }
    // Return an object of player entities
    getPlayerEntities() {
        return Engine.GuiInterfaceCall("GetPlayerEntities");
    }
    getNextRelay(entityId) {
        return this.getClosestEntity(entityId, this.getPlayerEntities());

    }
    getClosestEntity(entityId, entityList) {
        warn(JSON.stringify(entityId));
        // Get info on given entity
        const state = GetEntityState(entityId);
        //warn(JSON.stringify(state));
        try {
            const position = new Vector2D(state.rallyPoint.position.x, state.rallyPoint.position.z);

            // Initialize candidate target
            let closestCandidate = undefined;
            let distanceCandidate = Infinity;

            // Find closest candidate target
            for (let id of entityList) {
                try {
                    let targetState = GetEntityState(id);
                    //warn(JSON.stringify(targetState));
                    let targetPosition = new Vector2D(targetState.position.x, targetState.position.z);
                    let distance = position.distanceTo(targetPosition);
                    if (distance < distanceCandidate) {
                        distanceCandidate = distance;
                        closestCandidate = id;
                    }
                } catch (e) { }
            }
            warn(closestCandidate);
            return closestCandidate;
        } catch (e) { }
    }

    invertFromSelection() {
        const entityList = this.getPlayerEntities();
        const newEndEnt = g_Selection.filter(e => {
            let state = GetEntityState(e);
            return state && (!!state.garrisonHolder || !!state.turretHolder);
        });
        const nextRelayEnt = this.getClosestEntity(newEndEnt, entityList);
        let state = GetEntityState(nextRelayEnt);
        warn(JSON.stringify(newEndEnt) + " " + JSON.stringify(newEndEnt))
        /*g_UnitActions["set-rallypoint"].execute(
            state.position,
            {
                data: {
                    command: "garrison",
                    entities: newEndEnt[0]
                }
            },
            [nextRelayEnt[0]],
            false,
            false
        );*/
    }

    enableOnSelection() {
        const garrisonHolders = g_Selection.filter(e => {
            let state = GetEntityState(e);
            return state && !!state.garrisonHolder;
        });
        garrisonHolders.forEach(entity => {
            if (this.garrisonEnt.indexOf(entity) === -1)
                this.garrisonEnt.push(entity);
        });
        const turretHolders = g_Selection.filter(e => {
            let state = GetEntityState(e);
            return state && !!state.turretHolder;
        });
        turretHolders.forEach(entity => {
            if (this.turretEnt.indexOf(entity) === -1)
                this.turretEnt.push(entity);
        });


        warn(JSON.stringify(this.garrisonEnt));
    }
    disableOnSelection() {
        const garrisonHolders = g_Selection.filter(e => {
            let state = GetEntityState(e);
            return state && !!state.garrisonHolder;
        });
        garrisonHolders.forEach(entity => {
            const index = this.garrisonEnt.indexOf(entity);
            if (index !== -1) {
                this.garrisonEnt.splice(index, 1);
            }
        });
        const turretHolders = g_Selection.filter(e => {
            let state = GetEntityState(e);
            return state && !!state.turretHolder;
        });
        turretHolders.forEach(entity => {
            const index = this.garrisonEnt.indexOf(entity);
            if (index !== -1) {
                this.turretEnt.splice(index, 1);
            }
        });

        warn(JSON.stringify(this.garrisonEnt));
    }
    tickTasks() {
        let ownEnts = [];
        let otherEnts = [];

        for (let ent of this.garrisonEnt) {
            try {
                let state = GetEntityState(ent);
                if (state?.garrisonHolder?.entities[0] || Engine.ConfigDB_GetValue("user", "progui.stockfish.MaxSpeed") === "true" || this.turbo)
                    if (controlsPlayer(GetEntityState(ent).player))
                        ownEnts.push(ent);
                    else
                        otherEnts.push(ent);
            } catch (e) {
                warn("A StockFish chain has been broken!");
                this.garrisonEnt = this.garrisonEnt.filter(x => x != ent);
            }
            finally { continue; }
        }

        if (ownEnts.length)
            Engine.PostNetworkCommand({
                "type": "unload-all",
                "garrisonHolders": ownEnts
            });

        if (otherEnts.length)
            Engine.PostNetworkCommand({
                "type": "unload-all-by-owner",
                "garrisonHolders": otherEnts
            });
        for (let ent of this.turretEnt) {
            try {
                let state = GetEntityState(ent);
                if (state?.garrisonHolder?.entities[0] || Engine.ConfigDB_GetValue("user", "progui.stockfish.MaxSpeed") === "true")
                    if (controlsPlayer(GetEntityState(ent).player))
                        ownEnts.push(ent);
                    else
                        otherEnts.push(ent);
            } catch (e) {
                warn("A StockFish chain has been broken!");
                this.garrisonEnt = this.garrisonEnt.filter(x => x != ent);
            }
            finally { continue; }
        }


        let ownedHolders = [];
        let ejectables = [];
        for (let ent of this.turretEnt) {
            try {
                let turretHolderState = GetEntityState(ent);
                if (controlsPlayer(turretHolderState.player))
                    ownedHolders.push(ent);
                else {
                    for (let turret of turretHolderState.turretHolder.turretPoints.map(tp => tp.entity))
                        if (turret && controlsPlayer(GetEntityState(turret).player))
                            ejectables.push(turret);
                }
            } catch (e) {
                warn("A StockFish chain has been broken!");
                this.turretEnt = this.turretEnt.filter(x => x != ent);
            }
            finally { continue; }
        }
        if (ejectables.length)
            Engine.PostNetworkCommand({
                "type": "leave-turret",
                "entities": ejectables
            });

        if (ownedHolders.length)
            Engine.PostNetworkCommand({
                "type": "unload-turrets",
                "entities": ownedHolders
            });
    }
}